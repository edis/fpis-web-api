/*
SQLyog Community v13.1.1 (64 bit)
MySQL - 10.1.36-MariaDB : Database - fpis
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fpis` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `fpis`;

/*Table structure for table `angazovanje` */

DROP TABLE IF EXISTS `angazovanje`;

CREATE TABLE `angazovanje` (
  `tip_aktivnosti_id` bigint(20) NOT NULL,
  `nastavnik_id` bigint(20) NOT NULL,
  `stavka_pokrivenosti_nastave_id` bigint(20) NOT NULL,
  `pravo_potpisa` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`tip_aktivnosti_id`,`nastavnik_id`,`stavka_pokrivenosti_nastave_id`),
  KEY `nastavnik_id` (`nastavnik_id`),
  KEY `stavka_pokrivenosti_nastave_id` (`stavka_pokrivenosti_nastave_id`),
  CONSTRAINT `angazovanje_ibfk_1` FOREIGN KEY (`tip_aktivnosti_id`) REFERENCES `tip_aktivnosti` (`id`),
  CONSTRAINT `angazovanje_ibfk_2` FOREIGN KEY (`nastavnik_id`) REFERENCES `nastavnik` (`id`),
  CONSTRAINT `angazovanje_ibfk_3` FOREIGN KEY (`stavka_pokrivenosti_nastave_id`) REFERENCES `stavka_pokrivenosti_nastave` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `angazovanje` */

/*Table structure for table `literatura` */

DROP TABLE IF EXISTS `literatura`;

CREATE TABLE `literatura` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `predmet_id` bigint(20) DEFAULT NULL,
  `naziv` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `predmet_id` (`predmet_id`),
  CONSTRAINT `literatura_ibfk_1` FOREIGN KEY (`predmet_id`) REFERENCES `predmet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `literatura` */

insert  into `literatura`(`id`,`predmet_id`,`naziv`) values 
(1,1,'Baze podataka 1'),
(2,5,'Zbirka zadataka iz SPA'),
(4,3,'Java programiranje'),
(5,4,'Web programiranje'),
(6,4,'Javascript programiranje');

/*Table structure for table `literatura_stavka` */

DROP TABLE IF EXISTS `literatura_stavka`;

CREATE TABLE `literatura_stavka` (
  `literatura_id` bigint(20) NOT NULL,
  `stavka_pokrivenosti_nastave_id` bigint(20) NOT NULL,
  PRIMARY KEY (`literatura_id`,`stavka_pokrivenosti_nastave_id`),
  KEY `stavka_pokrivenosti_nastave_id` (`stavka_pokrivenosti_nastave_id`),
  CONSTRAINT `literatura_stavka_ibfk_1` FOREIGN KEY (`literatura_id`) REFERENCES `literatura` (`id`),
  CONSTRAINT `literatura_stavka_ibfk_2` FOREIGN KEY (`stavka_pokrivenosti_nastave_id`) REFERENCES `stavka_pokrivenosti_nastave` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `literatura_stavka` */

/*Table structure for table `nastavni_plan` */

DROP TABLE IF EXISTS `nastavni_plan`;

CREATE TABLE `nastavni_plan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `datum_od` date DEFAULT NULL,
  `datum_do` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `nastavni_plan` */

insert  into `nastavni_plan`(`id`,`datum_od`,`datum_do`) values 
(1,'2019-01-01','2019-12-31'),
(2,'2018-01-01','2018-12-31'),
(3,'2020-01-01','2020-12-31');

/*Table structure for table `nastavnik` */

DROP TABLE IF EXISTS `nastavnik`;

CREATE TABLE `nastavnik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ime` varchar(50) DEFAULT NULL,
  `prezime` varchar(50) DEFAULT NULL,
  `jmbg` varchar(13) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jmbg` (`jmbg`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `nastavnik` */

insert  into `nastavnik`(`id`,`ime`,`prezime`,`jmbg`) values 
(1,'Pera','Peric','123'),
(2,'Zika','Zikic','1234'),
(3,'Mika','Mikic','12345');

/*Table structure for table `pokrivenost_nastave` */

DROP TABLE IF EXISTS `pokrivenost_nastave`;

CREATE TABLE `pokrivenost_nastave` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `datum` date DEFAULT NULL,
  `godina` varchar(10) DEFAULT NULL,
  `nastavni_plan_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nastavni_plan_id` (`nastavni_plan_id`),
  CONSTRAINT `pokrivenost_nastave_ibfk_1` FOREIGN KEY (`nastavni_plan_id`) REFERENCES `nastavni_plan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pokrivenost_nastave` */

/*Table structure for table `predmet` */

DROP TABLE IF EXISTS `predmet`;

CREATE TABLE `predmet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `predmet` */

insert  into `predmet`(`id`,`naziv`) values 
(1,'Baze podataka'),
(2,'Programiranje 1'),
(3,'Programiranje 2'),
(4,'FPIS'),
(5,'Strukture podataka i algoritmi'),
(6,'Analiza i logicko projektovanje'),
(8,'OIKT');

/*Table structure for table `semestar` */

DROP TABLE IF EXISTS `semestar`;

CREATE TABLE `semestar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `redni_broj` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `semestar` */

insert  into `semestar`(`id`,`redni_broj`) values 
(1,1),
(2,2),
(3,3),
(4,4),
(5,5),
(6,6),
(7,7),
(8,8);

/*Table structure for table `stavka_pokrivenosti_nastave` */

DROP TABLE IF EXISTS `stavka_pokrivenosti_nastave`;

CREATE TABLE `stavka_pokrivenosti_nastave` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `predmet_id` bigint(20) NOT NULL,
  `pokrivenost_nastave_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`,`pokrivenost_nastave_id`),
  KEY `stavka_pokrivenosti_nastave_ibfk_1` (`predmet_id`),
  KEY `pokrivenost_nastave_id` (`pokrivenost_nastave_id`),
  CONSTRAINT `stavka_pokrivenosti_nastave_ibfk_1` FOREIGN KEY (`predmet_id`) REFERENCES `predmet` (`id`),
  CONSTRAINT `stavka_pokrivenosti_nastave_ibfk_2` FOREIGN KEY (`pokrivenost_nastave_id`) REFERENCES `pokrivenost_nastave` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `stavka_pokrivenosti_nastave` */

/*Table structure for table `tip_aktivnosti` */

DROP TABLE IF EXISTS `tip_aktivnosti`;

CREATE TABLE `tip_aktivnosti` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(50) DEFAULT NULL,
  `obavezna_prijava` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tip_aktivnosti` */

insert  into `tip_aktivnosti`(`id`,`naziv`,`obavezna_prijava`) values 
(1,'predavanja',0),
(2,'vezbe',0),
(3,'laboratorijske vezbe',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
