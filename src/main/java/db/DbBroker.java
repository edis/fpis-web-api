package db;

import java.io.Serializable;
import java.util.Collection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import model.Nastavnik;
import model.PokrivenostNastave;
import org.springframework.transaction.annotation.Propagation;

@Repository
@Scope(scopeName = "singleton")
public class DbBroker {

    @Autowired
    private SessionFactory sessionFactory;

    public DbBroker() {
    }

    @Transactional
    public Collection<Nastavnik> ucitajSveNastavnike() {
        Session session = sessionFactory.getCurrentSession();
        return session.createNamedQuery("Nastavnik.LoadAll").getResultList();
    }

    public Nastavnik kreirajNastavnika(Nastavnik nastavnik) {
        Session session = sessionFactory.getCurrentSession();
        Long nastavnikId = (Long) session.save(nastavnik);
        return session.load(Nastavnik.class, nastavnikId);
    }

    public Nastavnik ucitajNastavnika(Long nastavnikId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Nastavnik.class, nastavnikId);
    }

    public boolean obrisiNastavnika(Nastavnik nastavnik) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.delete(nastavnik);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Nastavnik azurirajNastavnika(Nastavnik nastavnik) {
        Session session = sessionFactory.getCurrentSession();
        session.update(nastavnik);
        return session.get(Nastavnik.class, nastavnik.getId());
    }

    public Collection loadGeneric(String beanName) {
        Session session = sessionFactory.getCurrentSession();
        return session.createNamedQuery(beanName + ".LoadAll").getResultList();
    }

    public <T extends Serializable> T loadById(Class clazz, Long id) {
        Session session = sessionFactory.getCurrentSession();
        return (T) session.get(clazz, id);
    }

    public void sacuvajPokrivenostNastave(PokrivenostNastave pokrivenostNastave) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(pokrivenostNastave);
    }

    public PokrivenostNastave ucitajPokrivenostNastavePoGodini(String godina) {
        Session session = sessionFactory.getCurrentSession();
        return session.byNaturalId(PokrivenostNastave.class).using("godina", godina).load();
    }

    public PokrivenostNastave ucitajPokrivenostNastave(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(PokrivenostNastave.class, id);
    }

    public boolean obrisiPokrivenostNastave(Long id) {
        Session session = sessionFactory.getCurrentSession();
        try {
            PokrivenostNastave pokrivenostNastave = session.get(PokrivenostNastave.class, id);
            session.delete(pokrivenostNastave);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
