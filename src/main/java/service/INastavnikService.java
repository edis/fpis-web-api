package service;

import java.util.Collection;

import dto.NastavnikDTO;
import model.Nastavnik;

public interface INastavnikService {

    Collection<Nastavnik> ucitajSve();

    Nastavnik ucitaj(Long id);

    Nastavnik sacuvaj(NastavnikDTO nastavnikDTO);

    Nastavnik obrisi(Long id);

    Nastavnik azuriraj(NastavnikDTO nastavnikDTO);
}
