/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dto.PokrivenostNastaveCreateDTO;
import java.io.Serializable;
import java.util.Collection;
import model.PokrivenostNastave;

/**
 *
 * @author edis
 */
public interface IPokrivenostNastaveService {

    Serializable save(PokrivenostNastaveCreateDTO pokrivenostNastaveDTO);

    Collection loadAll();

    PokrivenostNastave loadById(Long id);

    void update(PokrivenostNastaveCreateDTO pokrivenostNastaveDTO);

    void delete(Long id);
}
