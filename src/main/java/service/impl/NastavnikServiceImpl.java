package service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import db.DbBroker;
import dto.NastavnikDTO;
import model.Nastavnik;
import service.INastavnikService;
import validator.Validator;

@Service
public class NastavnikServiceImpl implements INastavnikService {

    @Autowired
    private DbBroker dbBroker;

    public Collection<Nastavnik> ucitajSve() {
        return dbBroker.ucitajSveNastavnike();
    }

    public Nastavnik sacuvaj(NastavnikDTO nastavnikDTO) {
        Validator.validate(nastavnikDTO);
        Nastavnik nastavnik = fromDTO(nastavnikDTO);
        return dbBroker.kreirajNastavnika(nastavnik);
    }

    public Nastavnik obrisi(Long id) {
        Nastavnik nastavnik = ucitaj(id);
        if (nastavnik == null) {
            throw new RuntimeException("Наставник не постоји.");
        }
        return dbBroker.obrisiNastavnika(nastavnik) ? nastavnik : null;
    }

    private Nastavnik fromDTO(NastavnikDTO nastavnikDTO) {
        Nastavnik nastavnik = new Nastavnik();
        nastavnik.setId(nastavnikDTO.getId());
        nastavnik.setIme(nastavnikDTO.getIme());
        nastavnik.setPrezime(nastavnikDTO.getPrezime());
        nastavnik.setJmbg(nastavnikDTO.getJmbg());
        return nastavnik;
    }

    @Transactional
    public Nastavnik ucitaj(Long id) {
        return dbBroker.ucitajNastavnika(id);
    }

    public Nastavnik azuriraj(NastavnikDTO nastavnikDTO) {
        Validator.validate(nastavnikDTO);
        Nastavnik nastavnik = ucitaj(nastavnikDTO.getId());
        if (nastavnik == null) {
            throw new RuntimeException("Наставник не постоји.");
        }
        nastavnik.setIme(nastavnikDTO.getIme());
        nastavnik.setPrezime(nastavnikDTO.getPrezime());
        return dbBroker.azurirajNastavnika(nastavnik);
    }
}
