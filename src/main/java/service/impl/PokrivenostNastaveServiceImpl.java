/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import db.DbBroker;
import dto.AngazovanjeDTO;
import dto.LiteraturaDTO;
import dto.NastavniPlanDTO;
import dto.NastavnikDTO;
import dto.PokrivenostNastaveCreateDTO;
import dto.PredmetDTO;
import dto.StavkaPokrivenostiNastaveDTO;
import dto.TipAktivnostiDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.Angazovanje;
import model.Literatura;
import model.NastavniPlan;
import model.Nastavnik;
import model.PokrivenostNastave;
import model.Predmet;
import model.StavkaPokrivenostiNastave;
import model.TipAktivnosti;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.IPokrivenostNastaveService;
import validator.Validator;

/**
 *
 * @author edis
 */
@Service
public class PokrivenostNastaveServiceImpl implements IPokrivenostNastaveService {

    @Autowired
    private DbBroker db;

    @Override
    @Transactional
    public Collection loadAll() {
        return db.loadGeneric("PokrivenostNastave");
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Serializable save(PokrivenostNastaveCreateDTO pokrivenostNastaveDTO) {
        Validator.validate(pokrivenostNastaveDTO);

        PokrivenostNastave postojecaPokrivenostNastave = db.ucitajPokrivenostNastavePoGodini(pokrivenostNastaveDTO.getGodina());
        if (postojecaPokrivenostNastave != null) {
            throw new RuntimeException("Покривеност наставе за " + pokrivenostNastaveDTO.getGodina() + " годину већ постоји.");
        }

        if (pokrivenostNastaveDTO.getDatum() == null) {
            throw new RuntimeException("Изабери датум за покривеност наставе");
        }

        NastavniPlanDTO nastavniPlanDTO = pokrivenostNastaveDTO.getNastavniPlan();

        if (nastavniPlanDTO == null) {
            throw new RuntimeException("Изабери наставни план");
        }

        NastavniPlan nastavniPlan = db.loadById(NastavniPlan.class, nastavniPlanDTO.getId());

        if (nastavniPlan == null) {
            throw new RuntimeException("Изабрани наставни план не постоји.");
        }

        PokrivenostNastave pokrivenostNastave = new PokrivenostNastave();
        pokrivenostNastave.setDatum(pokrivenostNastaveDTO.getDatum());
        pokrivenostNastave.setGodina(pokrivenostNastaveDTO.getGodina());
        pokrivenostNastave.setNastavniPlan(nastavniPlan);

        List<StavkaPokrivenostiNastave> stavke = new ArrayList<>();

        List<StavkaPokrivenostiNastaveDTO> stavkeDTO = pokrivenostNastaveDTO.getStavke();
        for (StavkaPokrivenostiNastaveDTO stavkaDTO : stavkeDTO) {
            StavkaPokrivenostiNastave stavkaPokrivenostiNastave = new StavkaPokrivenostiNastave();

            PredmetDTO predmetDTO = stavkaDTO.getPredmet();
            Predmet predmet = db.loadById(Predmet.class, predmetDTO.getId());
            if (predmet == null) {
                throw new RuntimeException("Предмет " + predmetDTO.getNaziv() + " не постоји");
            }

            stavkaPokrivenostiNastave.setPredmet(predmet);

            List<Angazovanje> angazovanja = new ArrayList<>();

            List<AngazovanjeDTO> angazovanjaDTO = stavkaDTO.getAngazovanja();
            for (AngazovanjeDTO angazovanjeDTO : angazovanjaDTO) {
                NastavnikDTO nastavnikDTO = angazovanjeDTO.getNastavnik();
                TipAktivnostiDTO tipAktivnostiDTO = angazovanjeDTO.getTipAktivnosti();

                if (nastavnikDTO == null) {
                    throw new RuntimeException("Изабери наставника за ангажовање");
                }

                if (tipAktivnostiDTO == null) {
                    throw new RuntimeException("Изабери тип активности за ангажовање");
                }

                Angazovanje angazovanje = new Angazovanje();
                angazovanje.setPravoPotpisa(angazovanjeDTO.getPravoPotpisa());
                angazovanje.setStavkaPokrivenostiNastave(stavkaPokrivenostiNastave);

                Nastavnik nastavnik = db.loadById(Nastavnik.class, angazovanjeDTO.getNastavnik().getId());
                if (nastavnik == null) {
                    throw new RuntimeException("Изабрани наставник не постоји (" + nastavnikDTO.getIme() + " " + nastavnikDTO.getPrezime() + ")");
                }
                angazovanje.setNastavnik(nastavnik);

                TipAktivnosti tipAktivnosti = db.loadById(TipAktivnosti.class, angazovanjeDTO.getTipAktivnosti().getId());
                if (tipAktivnosti == null) {
                    throw new RuntimeException("Изабрани тип активности за ангажовање не постоји");
                }
                angazovanje.setTipAktivnosti(tipAktivnosti);

                angazovanja.add(angazovanje);
            }

            List<Literatura> literature = new ArrayList<>();
            List<LiteraturaDTO> literatureDTO = stavkaDTO.getLiteratura();
            for (LiteraturaDTO literaturaDTO : literatureDTO) {
                Literatura literatura = db.loadById(Literatura.class, literaturaDTO.getId());
                if (literatura == null) {
                    throw new RuntimeException("Литература " + literaturaDTO.getNaziv() + " не постоји");
                }
                literature.add(literatura);
            }

            stavkaPokrivenostiNastave.setLiterature(literature);
            stavkaPokrivenostiNastave.setAngazovanja(angazovanja);
            stavkaPokrivenostiNastave.setPokrivenostNastave(pokrivenostNastave);
            stavke.add(stavkaPokrivenostiNastave);
        }

        pokrivenostNastave.setStavke(stavke);

        db.sacuvajPokrivenostNastave(pokrivenostNastave);
        return pokrivenostNastave.getId();
    }

    @Override
    public PokrivenostNastave loadById(Long id) {
        return db.ucitajPokrivenostNastave(id);
    }

    private PokrivenostNastave createPokrivenostNastave(PokrivenostNastaveCreateDTO pokrivenostNastaveDTO) {
        Validator.validate(pokrivenostNastaveDTO);

        PokrivenostNastave postojecaPokrivenostNastave = db.ucitajPokrivenostNastavePoGodini(pokrivenostNastaveDTO.getGodina());
        if (postojecaPokrivenostNastave != null && pokrivenostNastaveDTO.getId() != postojecaPokrivenostNastave.getId()) {
            throw new RuntimeException("Покривеност наставе за " + pokrivenostNastaveDTO.getGodina() + " годину већ постоји.");
        }

        if (pokrivenostNastaveDTO.getDatum() == null) {
            throw new RuntimeException("Изабери датум за покривеност наставе");
        }

        NastavniPlanDTO nastavniPlanDTO = pokrivenostNastaveDTO.getNastavniPlan();

        if (nastavniPlanDTO == null) {
            throw new RuntimeException("Изабери наставни план");
        }

        NastavniPlan nastavniPlan = db.loadById(NastavniPlan.class, nastavniPlanDTO.getId());

        if (nastavniPlan == null) {
            throw new RuntimeException("Изабрани наставни план не постоји.");
        }

        PokrivenostNastave pokrivenostNastave = new PokrivenostNastave();
        pokrivenostNastave.setDatum(pokrivenostNastaveDTO.getDatum());
        pokrivenostNastave.setGodina(pokrivenostNastaveDTO.getGodina());
        pokrivenostNastave.setNastavniPlan(nastavniPlan);

        List<StavkaPokrivenostiNastave> stavke = new ArrayList<>();

        List<StavkaPokrivenostiNastaveDTO> stavkeDTO = pokrivenostNastaveDTO.getStavke();
        for (StavkaPokrivenostiNastaveDTO stavkaDTO : stavkeDTO) {
            StavkaPokrivenostiNastave stavkaPokrivenostiNastave = new StavkaPokrivenostiNastave();

            PredmetDTO predmetDTO = stavkaDTO.getPredmet();
            Predmet predmet = db.loadById(Predmet.class, predmetDTO.getId());
            if (predmet == null) {
                throw new RuntimeException("Предмет " + predmetDTO.getNaziv() + " не постоји");
            }

            stavkaPokrivenostiNastave.setPredmet(predmet);

            List<Angazovanje> angazovanja = new ArrayList<>();

            List<AngazovanjeDTO> angazovanjaDTO = stavkaDTO.getAngazovanja();
            for (AngazovanjeDTO angazovanjeDTO : angazovanjaDTO) {
                NastavnikDTO nastavnikDTO = angazovanjeDTO.getNastavnik();
                TipAktivnostiDTO tipAktivnostiDTO = angazovanjeDTO.getTipAktivnosti();

                if (nastavnikDTO == null) {
                    throw new RuntimeException("Изабери наставника за ангажовање");
                }

                if (tipAktivnostiDTO == null) {
                    throw new RuntimeException("Изабери тип активности за ангажовање");
                }

                Angazovanje angazovanje = new Angazovanje();
                angazovanje.setPravoPotpisa(angazovanjeDTO.getPravoPotpisa());
                angazovanje.setStavkaPokrivenostiNastave(stavkaPokrivenostiNastave);

                Nastavnik nastavnik = db.loadById(Nastavnik.class, angazovanjeDTO.getNastavnik().getId());
                if (nastavnik == null) {
                    throw new RuntimeException("Изабрани наставник не постоји (" + nastavnikDTO.getIme() + " " + nastavnikDTO.getPrezime() + ")");
                }
                angazovanje.setNastavnik(nastavnik);

                TipAktivnosti tipAktivnosti = db.loadById(TipAktivnosti.class, angazovanjeDTO.getTipAktivnosti().getId());
                if (tipAktivnosti == null) {
                    throw new RuntimeException("Изабрани тип активности за ангажовање не постоји");
                }
                angazovanje.setTipAktivnosti(tipAktivnosti);

                angazovanja.add(angazovanje);
            }

            List<Literatura> literature = new ArrayList<>();
            List<LiteraturaDTO> literatureDTO = stavkaDTO.getLiteratura();
            for (LiteraturaDTO literaturaDTO : literatureDTO) {
                Literatura literatura = db.loadById(Literatura.class, literaturaDTO.getId());
                if (literatura == null) {
                    throw new RuntimeException("Литература " + literaturaDTO.getNaziv() + " не постоји");
                }
                literature.add(literatura);
            }

            stavkaPokrivenostiNastave.setLiterature(literature);
            stavkaPokrivenostiNastave.setAngazovanja(angazovanja);
            stavkaPokrivenostiNastave.setPokrivenostNastave(pokrivenostNastave);
            stavke.add(stavkaPokrivenostiNastave);
        }

        pokrivenostNastave.setStavke(stavke);
        return pokrivenostNastave;
    }
    
    @Override
    @Transactional
    public void update(PokrivenostNastaveCreateDTO pokrivenostNastaveDTO) {
        Validator.validate(pokrivenostNastaveDTO);

        Long id = pokrivenostNastaveDTO.getId();
        if (id == null || id <= 0) {
            throw new RuntimeException("ИД је неисправан.");
        }

        PokrivenostNastave postojecaPokrivenostNastave = db.ucitajPokrivenostNastavePoGodini(pokrivenostNastaveDTO.getGodina());
        if (postojecaPokrivenostNastave != null && postojecaPokrivenostNastave.getId() != id) {
            throw new RuntimeException("Покривеност наставе за " + pokrivenostNastaveDTO.getGodina() + " годину већ постоји.");
        }

        PokrivenostNastave novaPokrivenostNastave = createPokrivenostNastave(pokrivenostNastaveDTO);

        if (db.obrisiPokrivenostNastave(id)) {
            save(pokrivenostNastaveDTO);
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        PokrivenostNastave pokrivenostNastave = db.loadById(PokrivenostNastave.class, id);
        if (pokrivenostNastave == null) {
            throw new RuntimeException("Покривеност наставе не постоји.");
        }
        
        db.obrisiPokrivenostNastave(id);
    }
}
