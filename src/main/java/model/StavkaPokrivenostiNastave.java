/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

/**
 *
 * @author edis
 */
@Entity
@Table(name = "stavka_pokrivenosti_nastave")
public class StavkaPokrivenostiNastave implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "predmet_id")
    private Predmet predmet;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "literatura_stavka",
            joinColumns = @JoinColumn(name = "stavka_pokrivenosti_nastave_id"),
            inverseJoinColumns = @JoinColumn(name = "literatura_id"))
    private List<Literatura> literature;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "stavkaPokrivenostiNastave")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<Angazovanje> angazovanja;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pokrivenost_nastave_id")
    private PokrivenostNastave pokrivenostNastave;

    public StavkaPokrivenostiNastave() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Predmet getPredmet() {
        return predmet;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }

    public List<Literatura> getLiterature() {
        return literature;
    }

    public void setLiterature(List<Literatura> literature) {
        this.literature = literature;
    }

    public List<Angazovanje> getAngazovanje() {
        return angazovanja;
    }

    public void setAngazovanja(List<Angazovanje> angazovanja) {
        this.angazovanja = angazovanja;
    }

    public PokrivenostNastave getPokrivenostNastave() {
        return pokrivenostNastave;
    }

    public void setPokrivenostNastave(PokrivenostNastave pokrivenostNastave) {
        this.pokrivenostNastave = pokrivenostNastave;
    }
}
