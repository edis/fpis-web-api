/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.NaturalId;

/**
 *
 * @author edis
 */
@Entity
@Table(name = "pokrivenost_nastave")
@NamedQueries({
    @NamedQuery(name = "PokrivenostNastave.LoadAll", query = "FROM PokrivenostNastave ORDER BY id DESC")
})
public class PokrivenostNastave implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "datum")
    private LocalDate datum;

    @Column(name = "godina")
    @NaturalId
    private String godina;

    @JoinColumn(name = "nastavni_plan_id")
    @ManyToOne
    private NastavniPlan nastavniPlan;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY, mappedBy = "pokrivenostNastave")
    @Cascade({org.hibernate.annotations.CascadeType.DELETE, org.hibernate.annotations.CascadeType.REMOVE})
    private Collection<StavkaPokrivenostiNastave> stavke;

    public PokrivenostNastave() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getGodina() {
        return godina;
    }

    public void setGodina(String godina) {
        this.godina = godina;
    }

    public NastavniPlan getNastavniPlan() {
        return nastavniPlan;
    }

    public void setNastavniPlan(NastavniPlan nastavniPlan) {
        this.nastavniPlan = nastavniPlan;
    }

    public Collection<StavkaPokrivenostiNastave> getStavke() {
        return stavke;
    }

    public void setStavke(Collection<StavkaPokrivenostiNastave> stavke) {
        this.stavke = stavke;
    }
}
