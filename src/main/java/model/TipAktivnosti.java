/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author edis
 */
@Entity
@Table(name = "tip_aktivnosti")
@NamedQueries({
    @NamedQuery(name = "TipAktivnosti.LoadAll", query = "from TipAktivnosti")
})
public class TipAktivnosti implements Serializable {

    @Id
    @Column
    private Long id;

    @Column
    private String naziv;

    @Column(name = "obavezna_prijava")
    private short obaveznaPrijava;

    public TipAktivnosti() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public short getObaveznaPrijava() {
        return obaveznaPrijava;
    }

    public void setObaveznaPrijava(short obaveznaPrijava) {
        this.obaveznaPrijava = obaveznaPrijava;
    }

    @Override
    public String toString() {
        return "TipAktivnosti{" + "id=" + id + ", naziv=" + naziv + '}';
    }
}
