/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author edis
 */
@Entity
@Table(name = "angazovanje")
public class Angazovanje implements Serializable {

    @Column(name = "tip_aktivnosti_id", insertable = false, updatable = false)
    private Long tipAktivnostiID;

    @Column(name = "nastavnik_id", insertable = false, updatable = false)
    private Long nastavnikID;

    @Column(name = "stavka_pokrivenosti_nastave_id", insertable = false, updatable = false)
    private Long stavkaPokrivenostiNastaveID;

    @JoinColumn(name = "tip_aktivnosti_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @Id
    private TipAktivnosti tipAktivnosti;

    @JoinColumn(name = "nastavnik_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @Id
    private Nastavnik nastavnik;

    @JoinColumn(name = "stavka_pokrivenosti_nastave_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @Id
    private StavkaPokrivenostiNastave stavkaPokrivenostiNastave;

    @Column(name = "pravo_potpisa")
    private Integer pravoPotpisa;

    public Angazovanje() {
    }

    public Long getTipAktivnostiID() {
        return tipAktivnostiID;
    }

    public void setTipAktivnostiID(Long tipAktivnostiID) {
        this.tipAktivnostiID = tipAktivnostiID;
    }

    public Long getNastavnikID() {
        return nastavnikID;
    }

    public void setNastavnikID(Long nastavnikID) {
        this.nastavnikID = nastavnikID;
    }

    public TipAktivnosti getTipAktivnosti() {
        return tipAktivnosti;
    }

    public void setTipAktivnosti(TipAktivnosti tipAktivnosti) {
        this.tipAktivnosti = tipAktivnosti;
    }

    public Nastavnik getNastavnik() {
        return nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }

    public Long getStavkaPokrivenostiNastaveID() {
        return stavkaPokrivenostiNastaveID;
    }

    public void setStavkaPokrivenostiNastaveID(Long stavkaPokrivenostiNastaveID) {
        this.stavkaPokrivenostiNastaveID = stavkaPokrivenostiNastaveID;
    }

    public StavkaPokrivenostiNastave getStavkaPokrivenostiNastave() {
        return stavkaPokrivenostiNastave;
    }

    public void setStavkaPokrivenostiNastave(StavkaPokrivenostiNastave stavkaPokrivenostiNastave) {
        this.stavkaPokrivenostiNastave = stavkaPokrivenostiNastave;
    }

    public Integer getPravoPotpisa() {
        return pravoPotpisa;
    }

    public void setPravoPotpisa(Integer pravoPotpisa) {
        this.pravoPotpisa = pravoPotpisa;
    }

    @Override
    public String toString() {
        return "Angazovanje{" + "tipAktivnosti=" + tipAktivnosti + ", nastavnik=" + nastavnik + ", stavkaPokrivenostiNastave=" + stavkaPokrivenostiNastave + ", pravoPotpisa=" + pravoPotpisa + '}';
    }
}
