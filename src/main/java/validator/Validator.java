package validator;

import java.io.Serializable;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;

public class Validator {

    public static <T extends Serializable> void validate(T entity) {
        Set<ConstraintViolation<T>> errors = Validation.buildDefaultValidatorFactory().getValidator().validate(entity);
        if (!errors.isEmpty()) {
            throw new RuntimeException(errors.iterator().next().getMessage());
        }
    }
}
