package api.rest.controller;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dto.NastavnikDTO;
import model.Nastavnik;
import response.IResponse;
import response.ResponseBuilder;
import service.INastavnikService;

@RestController
@RequestMapping("nastavnik")
public class NastavnikController {

    @Autowired
    private INastavnikService service;

    @GetMapping
    public IResponse loadAll() {
        try {
            Collection<Nastavnik> nastavnici = service.ucitajSve();
            Collection<NastavnikDTO> nastavniciDTO = nastavnici.stream().map(nastavnik -> new NastavnikDTO(nastavnik)).collect(Collectors.toList());
            return ResponseBuilder.ok(nastavniciDTO);
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public IResponse loadById(@PathVariable(name = "id") Long id) {
        try {
            Nastavnik nastavnik = service.ucitaj(id);
            NastavnikDTO nastavnikDTO = new NastavnikDTO(nastavnik);
            return ResponseBuilder.ok(nastavnikDTO);
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }

    @PostMapping
    @Transactional
    public IResponse create(@RequestBody NastavnikDTO nastavnikDTO) {
        try {
            Nastavnik nastavnik = service.sacuvaj(nastavnikDTO);
            return ResponseBuilder.ok(new NastavnikDTO(nastavnik));
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @Transactional
    public IResponse update(@PathVariable(name = "id") Long id, @RequestBody NastavnikDTO nastavnikDTO) {
        try {
            if (id != nastavnikDTO.getId()) {
                throw new RuntimeException("Грешка. Погрешан објекат.");
            }

            Nastavnik nastavnik = service.azuriraj(nastavnikDTO);
            return ResponseBuilder.ok(new NastavnikDTO(nastavnik));
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @Transactional
    public IResponse delete(@PathVariable(name = "id") Long nastavnikId) {
        try {
            Nastavnik nastavnik = service.obrisi(nastavnikId);
            return ResponseBuilder.ok(new NastavnikDTO(nastavnik));
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }
}
