/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.controller;

import db.DbBroker;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import response.IResponse;
import response.ResponseBuilder;

/**
 *
 * @author edis
 */
@RestController
@RequestMapping("bean")
public class BeanController {

    @Autowired
    private DbBroker db;

    @GetMapping("/{beanName}")
    @Transactional
    public IResponse loadAll(@PathVariable(name = "beanName") String beanName) {
        try {
            Collection result = db.loadGeneric(beanName);
            String dtoClassName = "dto." + beanName + "DTO";
            Class dtoClass = Class.forName(dtoClassName);

            List dtoList = (List) result.stream().map(entity -> {
                try {
                    Constructor dtoClassConstructor = dtoClass.getConstructor(entity.getClass());
                    return dtoClassConstructor.newInstance(entity);
                } catch (Exception ex) {
                    return null;
                }
            }).collect(Collectors.toList());

            return ResponseBuilder.ok(dtoList);
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }
}
