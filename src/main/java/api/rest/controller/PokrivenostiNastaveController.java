/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.rest.controller;

import dto.PokrivenostNastaveCreateDTO;
import dto.PokrivenostNastaveDTO;
import java.io.Serializable;
import model.PokrivenostNastave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import response.IResponse;
import response.ResponseBuilder;
import service.IPokrivenostNastaveService;

/**
 *
 * @author edis
 */
@RestController
@RequestMapping("pokrivenost-nastave")
public class PokrivenostiNastaveController {

    @Autowired
    private IPokrivenostNastaveService service;

    @PostMapping
    public IResponse save(@RequestBody PokrivenostNastaveCreateDTO pokrivenostNastaveDTO) {
        try {
            Serializable id = service.save(pokrivenostNastaveDTO);
            return ResponseBuilder.ok(id);
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }
    
    @PutMapping
    public IResponse update(@RequestBody PokrivenostNastaveCreateDTO pokrivenostNastaveDTO) {
        try {
            service.update(pokrivenostNastaveDTO);
            return ResponseBuilder.ok("SUCCESS");
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }
    
    @DeleteMapping("/{id}")
    public IResponse delete(@PathVariable(name = "id") Long id) {
        try {
            service.delete(id);
            return ResponseBuilder.ok("SUCCESS");
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }
    
    @GetMapping("/{id}")
    @Transactional
    public IResponse loadById(@PathVariable(name = "id") Long id) {
        try {
            PokrivenostNastave pokrivenostNastave = service.loadById(id);
            return ResponseBuilder.ok(new PokrivenostNastaveDTO(pokrivenostNastave));
        } catch (Exception e) {
            return ResponseBuilder.error(e.getMessage());
        }
    }
}
