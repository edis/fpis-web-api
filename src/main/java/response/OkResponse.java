package response;

import java.util.Collection;

public class OkResponse implements IResponse {

    private int status;
    private int count;
    private Object data;

    public OkResponse() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCount() {
        if (data instanceof Collection) {
            return ((Collection) data).size();
        }
        return 1;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
