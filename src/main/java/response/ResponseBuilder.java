package response;

public class ResponseBuilder {

    public static IResponse ok(Object data) {
        OkResponse resp = new OkResponse();
        resp.setStatus(200);
        resp.setData(data);
        return resp;
    }

    public static IResponse error(String message) {
        ErrorResponse resp = new ErrorResponse();
        resp.setStatus(400);
        resp.setMessage(message);
        return resp;
    }

}
