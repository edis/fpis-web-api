/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import model.Literatura;

/**
 *
 * @author edis
 */
public class LiteraturaDTO implements Serializable {

    @NotNull(message = "Литература мора имати ИД")
    private Long id;
    private PredmetDTO predmet;
    private String naziv;

    public LiteraturaDTO(Literatura literatura) {
        this(literatura.getId(), new PredmetDTO(literatura.getPredmet()), literatura.getNaziv());
    }

    public LiteraturaDTO(Long id, PredmetDTO predmet, String naziv) {
        this.id = id;
        this.predmet = predmet;
        this.naziv = naziv;
    }

    public LiteraturaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PredmetDTO getPredmet() {
        return predmet;
    }

    public void setPredmet(PredmetDTO predmet) {
        this.predmet = predmet;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
