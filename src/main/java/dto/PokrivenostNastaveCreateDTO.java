/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 *
 * @author edis
 */
public class PokrivenostNastaveCreateDTO implements Serializable {

    private Long id;

    @NotNull(message = "Изаберите наставни план")
    private NastavniPlanDTO nastavniPlan;

    private List<StavkaPokrivenostiNastaveDTO> stavke;

    @NotNull
    private LocalDate datum;

    private String godina;

    public PokrivenostNastaveCreateDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NastavniPlanDTO getNastavniPlan() {
        return nastavniPlan;
    }

    public void setNastavniPlan(NastavniPlanDTO nastavniPlan) {
        this.nastavniPlan = nastavniPlan;
    }

    public List<StavkaPokrivenostiNastaveDTO> getStavke() {
        return stavke;
    }

    public void setStavke(List<StavkaPokrivenostiNastaveDTO> stavke) {
        this.stavke = stavke;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        try {
            this.datum = LocalDate.parse(datum, DateTimeFormatter.ISO_DATE);
            return;
        } catch (Exception e) {
        }
        
        try {
            this.datum = LocalDate.parse(datum, DateTimeFormatter.ISO_LOCAL_DATE);
            return;
        } catch (Exception e) {
        }
        
        try {
            this.datum = LocalDate.parse(datum, DateTimeFormatter.ofPattern("dd.MM.yyyy."));
            return;
        } catch (Exception e) {
            throw e;
        }
    }

    public String getGodina() {
        return godina;
    }

    public void setGodina(String godina) {
        this.godina = godina;
    }
}
