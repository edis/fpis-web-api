/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import model.Predmet;

/**
 *
 * @author edis
 */
public class PredmetDTO implements Serializable {

    @NotNull(message = "ИД предмета је обавезан")
    private Long id;

    @NotBlank(message = "Назив предмета је обавезан")
    private String naziv;

    public PredmetDTO() {
    }

    public PredmetDTO(Predmet predmet) {
        this(predmet.getId(), predmet.getNaziv());
    }

    public PredmetDTO(Long id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
