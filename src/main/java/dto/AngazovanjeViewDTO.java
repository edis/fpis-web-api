/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import model.Angazovanje;

/**
 *
 * @author edis
 */
public class AngazovanjeViewDTO implements Serializable {

    private NastavnikDTO nastavnik;
    private TipAktivnostiDTO tipAktivnosti;
    private boolean pravoPotpisa;

    public AngazovanjeViewDTO(Angazovanje angazovanje) {
        this(new NastavnikDTO(angazovanje.getNastavnik()), new TipAktivnostiDTO(angazovanje.getTipAktivnosti()), angazovanje.getPravoPotpisa() == 1 ? true: false);
    }
    
    public AngazovanjeViewDTO(NastavnikDTO nastavnik, TipAktivnostiDTO tipAktivnosti, boolean pravoPotpisa) {
        this.nastavnik = nastavnik;
        this.tipAktivnosti = tipAktivnosti;
        this.pravoPotpisa = pravoPotpisa;
    }

    public AngazovanjeViewDTO() {
    }

    public NastavnikDTO getNastavnik() {
        return nastavnik;
    }

    public void setNastavnik(NastavnikDTO nastavnik) {
        this.nastavnik = nastavnik;
    }

    public TipAktivnostiDTO getTipAktivnosti() {
        return tipAktivnosti;
    }

    public void setTipAktivnosti(TipAktivnostiDTO tipAktivnosti) {
        this.tipAktivnosti = tipAktivnosti;
    }

    public boolean isPravoPotpisa() {
        return pravoPotpisa;
    }

    public int getPravoPotpisa() {
        return pravoPotpisa ? 1 : 0;
    }

    public void setPravoPotpisa(boolean pravoPotpisa) {
        this.pravoPotpisa = pravoPotpisa;
    }
}
