/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import model.PokrivenostNastave;

/**
 *
 * @author edis
 */
public class PokrivenostNastaveDTO implements Serializable {

    private Long id;
    private NastavniPlanDTO nastavniPlan;
    private List<StavkaPokrivenostiNastaveViewDTO> stavke;
    private LocalDate datum;
    private String godina;

    public PokrivenostNastaveDTO(PokrivenostNastave pokrivenostNastave) {
        setId(pokrivenostNastave.getId());
        setNastavniPlan(new NastavniPlanDTO(pokrivenostNastave.getNastavniPlan()));
        setStavke(pokrivenostNastave.getStavke().stream().map(stavka -> new StavkaPokrivenostiNastaveViewDTO(stavka)).collect(Collectors.toList()));
        setDatum(pokrivenostNastave.getDatum());
        setGodina(pokrivenostNastave.getGodina());
    }

    public PokrivenostNastaveDTO(Long id, NastavniPlanDTO nastavniPlan, List<StavkaPokrivenostiNastaveViewDTO> stavke, LocalDate datum, String godina) {
        this.id = id;
        this.nastavniPlan = nastavniPlan;
        this.stavke = stavke;
        this.datum = datum;
        this.godina = godina;
    }

    public PokrivenostNastaveDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NastavniPlanDTO getNastavniPlan() {
        return nastavniPlan;
    }

    public void setNastavniPlan(NastavniPlanDTO nastavniPlan) {
        this.nastavniPlan = nastavniPlan;
    }

    public List<StavkaPokrivenostiNastaveViewDTO> getStavke() {
        return stavke;
    }

    public void setStavke(List<StavkaPokrivenostiNastaveViewDTO> stavke) {
        this.stavke = stavke;
    }

    public String getDatum() {
        return datum.format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
    }
    
    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getGodina() {
        return godina;
    }
    
    public void setGodina(String godina) {
        this.godina = godina;
    }
}
