/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import model.Semestar;

/**
 *
 * @author edis
 */
public class SemestarDTO implements Serializable {

    private Long id;
    private Integer redniBroj;

    public SemestarDTO(Semestar semestar) {
        this(semestar.getId(), semestar.getRedniBroj());
    }

    public SemestarDTO(Long id, Integer redniBroj) {
        this.id = id;
        this.redniBroj = redniBroj;
    }

    public SemestarDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRedniBroj() {
        return redniBroj;
    }

    public void setRedniBroj(Integer redniBroj) {
        this.redniBroj = redniBroj;
    }
}
