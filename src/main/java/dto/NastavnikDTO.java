package dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.sun.istack.NotNull;

import model.Nastavnik;

public class NastavnikDTO implements Serializable {

    private Long id;

    @NotNull
    @NotBlank(message = "Име не сме бити празно")
    private String ime;

    @NotNull
    @NotBlank(message = "Презиме не сме бити празно")
    private String prezime;

    @NotNull
    @NotBlank(message = "ЈМБГ не сме бити празан")
    private String jmbg;

    public NastavnikDTO() {
    }

    public NastavnikDTO(Nastavnik nastavnik) {
        this(nastavnik.getId(), nastavnik.getIme(), nastavnik.getPrezime(), nastavnik.getJmbg());
    }

    private NastavnikDTO(Long id, String ime, String prezime, String jmbg) {
        super();
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.jmbg = jmbg;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }
}
