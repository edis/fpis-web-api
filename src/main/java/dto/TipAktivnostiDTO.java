/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import model.TipAktivnosti;

/**
 *
 * @author edis
 */
public class TipAktivnostiDTO implements Serializable {

    private Long id;
    private String naziv;
    private short obaveznaPrijava;

    public TipAktivnostiDTO(TipAktivnosti tipAktivnosti) {
        this(tipAktivnosti.getId(), tipAktivnosti.getNaziv(), tipAktivnosti.getObaveznaPrijava());
    }

    public TipAktivnostiDTO(Long id, String naziv, short obaveznaPrijava) {
        this.id = id;
        this.naziv = naziv;
        this.obaveznaPrijava = obaveznaPrijava;
    }

    public TipAktivnostiDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public short getObaveznaPrijava() {
        return obaveznaPrijava;
    }

    public void setObaveznaPrijava(short obaveznaPrijava) {
        this.obaveznaPrijava = obaveznaPrijava;
    }
}
