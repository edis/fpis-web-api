/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;

/**
 *
 * @author edis
 */
public class AngazovanjeDTO implements Serializable {

    private NastavnikDTO nastavnik;
    private TipAktivnostiDTO tipAktivnosti;
    private boolean pravoPotpisa;

    public AngazovanjeDTO() {
    }

    public NastavnikDTO getNastavnik() {
        return nastavnik;
    }

    public void setNastavnik(NastavnikDTO nastavnik) {
        this.nastavnik = nastavnik;
    }

    public TipAktivnostiDTO getTipAktivnosti() {
        return tipAktivnosti;
    }

    public void setTipAktivnosti(TipAktivnostiDTO tipAktivnosti) {
        this.tipAktivnosti = tipAktivnosti;
    }

    public boolean isPravoPotpisa() {
        return pravoPotpisa;
    }

    public int getPravoPotpisa() {
        return pravoPotpisa ? 1 : 0;
    }

    public void setPravoPotpisa(boolean pravoPotpisa) {
        this.pravoPotpisa = pravoPotpisa;
    }
}
