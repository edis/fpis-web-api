/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author edis
 */
public class StavkaPokrivenostiNastaveDTO implements Serializable {

    private PredmetDTO predmet;
    private List<LiteraturaDTO> literatura;
    private List<AngazovanjeDTO> angazovanja;

    public StavkaPokrivenostiNastaveDTO() {
    }

    public PredmetDTO getPredmet() {
        return predmet;
    }

    public void setPredmet(PredmetDTO predmet) {
        this.predmet = predmet;
    }

    public List<LiteraturaDTO> getLiteratura() {
        return literatura;
    }

    public void setLiteratura(List<LiteraturaDTO> literatura) {
        this.literatura = literatura;
    }

    public List<AngazovanjeDTO> getAngazovanja() {
        return angazovanja;
    }

    public void setAngazovanja(List<AngazovanjeDTO> angazovanja) {
        this.angazovanja = angazovanja;
    }
}
