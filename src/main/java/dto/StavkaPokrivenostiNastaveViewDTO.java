/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import model.Angazovanje;
import model.Literatura;
import model.StavkaPokrivenostiNastave;

/**
 *
 * @author edis
 */
public class StavkaPokrivenostiNastaveViewDTO implements Serializable {

    private Long id;
    private PredmetDTO predmet;
    private List<LiteraturaDTO> literatura;
    private List<AngazovanjeViewDTO> angazovanja;

    public StavkaPokrivenostiNastaveViewDTO(StavkaPokrivenostiNastave stavkaPokrivenostiNastave) {
        setId(stavkaPokrivenostiNastave.getId());
        setPredmet(new PredmetDTO(stavkaPokrivenostiNastave.getPredmet()));
        setLiteratura(mapToLiteraturaDTO(stavkaPokrivenostiNastave.getLiterature()));
        setAngazovanja(mapToAngazovanjaDTO(stavkaPokrivenostiNastave.getAngazovanje()));
    }

    private List mapToLiteraturaDTO(List<Literatura> literature) {
        return literature.stream().map(literatura -> new LiteraturaDTO(literatura)).collect(Collectors.toList());
    }

    private List mapToAngazovanjaDTO(List<Angazovanje> angazovanja) {
        return angazovanja.stream().map(angazovanje -> new AngazovanjeViewDTO(angazovanje)).collect(Collectors.toList());
    }

    public StavkaPokrivenostiNastaveViewDTO(PredmetDTO predmet, List<LiteraturaDTO> literatura, List<AngazovanjeViewDTO> angazovanja) {
        this.predmet = predmet;
        this.literatura = literatura;
        this.angazovanja = angazovanja;
    }

    public StavkaPokrivenostiNastaveViewDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PredmetDTO getPredmet() {
        return predmet;
    }

    public void setPredmet(PredmetDTO predmet) {
        this.predmet = predmet;
    }

    public List<LiteraturaDTO> getLiteratura() {
        return literatura;
    }

    public void setLiteratura(List<LiteraturaDTO> literatura) {
        this.literatura = literatura;
    }

    public List<AngazovanjeViewDTO> getAngazovanja() {
        return angazovanja;
    }

    public void setAngazovanja(List<AngazovanjeViewDTO> angazovanja) {
        this.angazovanja = angazovanja;
    }
}
