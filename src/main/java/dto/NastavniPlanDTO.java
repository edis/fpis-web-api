/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import model.NastavniPlan;

/**
 *
 * @author edis
 */
public class NastavniPlanDTO implements Serializable {

    private Long id;
    private LocalDate datumOd;
    private LocalDate datumDo;

    public NastavniPlanDTO() {
    }

    public NastavniPlanDTO(NastavniPlan nastavniPlan) {
        this(nastavniPlan.getId(), nastavniPlan.getDatumOd(), nastavniPlan.getDatumDo());
    }

    public NastavniPlanDTO(Long id, LocalDate datumOd, LocalDate datumDo) {
        this.id = id;
        this.datumOd = datumOd;
        this.datumDo = datumDo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatumOd() {
        return datumOd.format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
    }

    public void setDatumOd(String datumOd) {
        this.datumOd = LocalDate.parse(datumOd, DateTimeFormatter.ofPattern("dd.MM.yyyy."));
    }

    public String getDatumDo() {
        return datumDo.format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
    }

    public void setDatumDo(String datumDo) {
        this.datumDo = LocalDate.parse(datumDo, DateTimeFormatter.ofPattern("dd.MM.yyyy."));
    }
}
